import ForkMe from "./ForkMe";
import Profile from "./Profile";
import RSSList from "./RSSList";

function App() {
    return (
        <div>
            <Profile/>
            <RSSList/>
            <ForkMe/>
        </div>
    )
}

export default App
